#define PIN 10

void pullDown() {
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN, LOW);
}

void release() {
  pinMode(PIN, INPUT);
}

bool reset() {
  pullDown();
  delayMicroseconds(500);
  release();
  delayMicroseconds(60);
  bool result = digitalRead(PIN) == LOW;
  delayMicroseconds(500);

  return result;
}

byte readBit() {
  pullDown();
  delayMicroseconds(1);
  release();
  delayMicroseconds(7);
  byte result = (digitalRead(PIN) == HIGH) ? 1 : 0;
  delayMicroseconds(52);
    
  return result;
}

byte readByte() {
  byte result = 0;
  for (byte i = 0; i < 8; i++) {
    result += readBit() << i;
  }
    
  return result;
}

void writeOne() {
  pullDown();
  delayMicroseconds(1);
  release();
  delayMicroseconds(59);
}

void writeZero() {
  pullDown();
  delayMicroseconds(59);
  release();
  delayMicroseconds(1);
}

void writeByte(byte data) {
  for (byte i = 0; i < 8; i++) {
    if (data & 1) {
      writeOne();
    }
    else {
      writeZero();
    }
    data >>= 1;
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  release();

  Serial.println("Setup done");
}

void sensorWrite(byte function) {
  reset();
  // skip ROM command
  writeByte(0xCC);
  // memory function command
  writeByte(function);
}

int16_t readTemp() {
  // convert temperature
  sensorWrite(0x44);
  delay(1000);

  // read scratchpad
  sensorWrite(0xBE);
  byte lsb = readByte();
  byte msb = readByte();
  
  int16_t temp = (msb << 4) + (lsb >> 4);
   
  return temp;
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(readTemp());
  delay(1000);
}
